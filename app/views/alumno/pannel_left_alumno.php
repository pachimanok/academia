<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="index.html"><i class="menu-icon fa fa-laptop"></i>ESCRITORIO</a>
                    </li>
                    <li class="menu-title">Academico</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>Mis Cursos</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-bars"></i><a href="#">Materia 1</a></li>
                            <li><i class="fa fa-bars"></i><a href="#">Materia 2</a></li>
                            <li><i class="fa fa-bars"></i><a href="#">Materia 3</a></li>
                            <li><i class="fa fa-bars"></i><a href="#">Materia 4</a></li>
                            <li><i class="fa fa-bars"></i><a href="#">Materia 5</a></li>
                            <li><i class="fa fa-bars"></i><a href="#">Materia 6</a></li>
                            <li><i class="fa fa-bars"></i><a href="#">Materia 7</a></li>
                            <li><i class="fa fa-bars"></i><a href="#">Materia 8</a></li>
                            <li><i class="fa fa-bars"></i><a href="#">Materia 9</a></li>
                            <li><i class="fa fa-bars"></i><a href="#">Materia 10</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Gestiones</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="#">InscribirMataria</a></li>
                            <li><i class="fa fa-table"></i><a href="#">Inscribir Examen</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Orden</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="forms-basic.html">Calendario</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="forms-advanced.html">Detalles de Notas</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-title">ADMINISTRATIVO</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-dollar"></i>Pagos</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-dollar"></i><a href="#">Pagar</a></li>
                            <li><i class="menu-icon fa fa-dollar"></i><a href="#">Emitir Boleto</a>
                            <li><i class="menu-icon fa fa-dollar"></i><a href="#">Cuenta Corriente</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-dollar"></i>Certificados</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-dollar"></i><a href="#">Fin de Curso </a></li>
                            <li><i class="menu-icon fa fa-dollar"></i><a href="#">Curso Aprobado</a>
                            <li><i class="menu-icon fa fa-dollar"></i><a href="#">Certificado General</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> <i class="menu-icon ti-email"></i>Mi Programa</a>
                    </li>
                    <!--li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Charts</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-line-chart"></i><a href="charts-chartjs.html">Chart JS</a>
                            </li>
                            <li><i class="menu-icon fa fa-area-chart"></i><a href="charts-flot.html">Flot Chart</a></li>
                            <li><i class="menu-icon fa fa-pie-chart"></i><a href="charts-peity.html">Peity Chart</a>
                            </li>
                        </ul>
                    </li-->
                    <!--li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Maps</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-map-o"></i><a href="maps-gmap.html">Google Maps</a></li>
                            <li><i class="menu-icon fa fa-street-view"></i><a href="maps-vector.html">Vector Maps</a>
                            </li>
                        </ul>
                    </li-->
                    <li class="menu-title">COMUNICACION</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Comunidad</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-login.html">Foro</a></li>
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-register.html">Preguntas al
                                    Profesor</a></li>
                            <li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.html">Preguntas
                                    Administración</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <!-- /#left-panel -->