<?php include('../fijos/header.php');?>
<?php include('../alumno/pannel_left_alumno.php');?>

<?php include('../fijos/pannel_right.php');?>
<div class="container">
    <div class="alerts pt-4">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="sufee-alert alert with-close alert-primary alert-dismissible fade show">
                    <span class="badge badge-pill badge-primary">Recuerda:</span>
                    Aun no tiene cancelada la cuota de Febrero.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Mis Materias</strong>
                        </div>
                        <div class="table-stats order-table ov-h">
                            <table class="table ">
                                <thead>
                                    <tr>
                                        <th class="serial">#</th>
                                        <th>tipo</th>
                                        <th>Materia</th>
                                        <th>Profesor</th>
                                        <th>Duracion</th>
                                        <th>Status</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="serial">1.</td>
                                        <td>OMI</td>
                                        <td>FAMILIARIZACIÓN BUQUES TANQUE</td>
                                        <td><span class="name">Louis Stanley</span></td>
                                        <td><span class="product"> 3 h.15 m.</span> </td>
                                        <td><span class="badge badge-complete">Aprobada</span></td>
                                        <td>
                                            <div class="button-group">
                                                <button class="btn btn-primary "><i class="fa fa-eye"></i></button>
                                                <button class="btn btn-primary "><i
                                                        class="fa fa-file-text-o"></i></button>
                                                <button class="btn btn-primary "><i
                                                        class="fa fa-envelope-o"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="serial">2.</td>
                                        <td>OMI</td>
                                        <td>NAV. POR RADAR, PLOTEODE RADAR Y USO DE APRA</td>
                                        <td><span class="name">John Doe</span></td>
                                        <td><span class="product"> 5 h.15 m.</span> </td>
                                        <td><span class="badge badge-warning">cursando</span></td>
                                        <td>
                                            <div class="button-group">
                                                <button class="btn btn-primary "><i class="fa fa-eye"></i></button>
                                                <button class="btn btn-primary "><i
                                                        class="fa fa-file-text-o"></i></button>
                                                <button class="btn btn-primary "><i
                                                        class="fa fa-envelope-o"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="serial">3.</td>
                                        <td>OMI</td>
                                        <td>NAVEGACIÓN POR RADAR Y PROTEO DE RADAR</td>
                                        <td><span class="name">Louis Midas</span></td>
                                        <td><span class="product"> 1 h.45 m.</span> </td>
                                        <td><span class="badge badge-pending">No Cursado</span></td>
                                        <td>
                                            <div class="button-group">
                                                <button class="btn btn-primary "><i class="fa fa-eye"></i></button>
                                                <button class="btn btn-primary "><i
                                                        class="fa fa-file-text-o"></i></button>
                                                <button class="btn btn-primary "><i
                                                        class="fa fa-envelope-o"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="serial">4.</td>
                                        <td>OMI</td>
                                        <td>CARGAS PELIGROSAS,PMENTE PELIGROSAS Y P</td>
                                        <td><span class="name">Julio Stanley</span></td>
                                        <td><span class="product"> 3 h.15 m.</span> </td>
                                        <td><span class="badge badge-complete">Aprobada</span></td>
                                        <td>
                                            <div class="button-group">
                                                <button class="btn btn-primary "><i class="fa fa-eye"></i></button>
                                                <button class="btn btn-primary "><i
                                                        class="fa fa-file-text-o"></i></button>
                                                <button class="btn btn-primary "><i
                                                        class="fa fa-envelope-o"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="serial">5.</td>
                                        <td>OMI</td>
                                        <td>PRIMEROS AUXILIOS BÁSICOS</td>
                                        <td><span class="name">Jennifer Verne</span></td>
                                        <td><span class="product"> 0 h.45 m.</span> </td>
                                        <td><span class="badge badge-warning">Cursado</span></td>
                                        <td>
                                            <div class="button-group">
                                                <button class="btn btn-primary "><i class="fa fa-eye"></i></button>
                                                <button class="btn btn-primary "><i
                                                        class="fa fa-file-text-o"></i></button>
                                                <button class="btn btn-primary "><i
                                                        class="fa fa-envelope-o"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="serial">1.</td>
                                        <td>OMI</td>
                                        <td>FAMILIARIZACIÓN BUQUES TANQUE</td>
                                        <td><span class="name">Louis Stanley</span></td>
                                        <td><span class="product"> 3 h.15 m.</span> </td>
                                        <td><span class="badge badge-complete">Aprobada</span></td>
                                        <td>
                                            <div class="button-group">
                                                <button class="btn btn-primary "><i class="fa fa-eye"></i></button>
                                                <button class="btn btn-primary "><i
                                                        class="fa fa-file-text-o"></i></button>
                                                <button class="btn btn-primary "><i
                                                        class="fa fa-envelope-o"></i></button>
                                            </div>
                                        </td>
                                    </tr>

                                    </tr>
                                </tbody>
                            </table>
                        </div> <!-- /.table-stats -->
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-one">
                                <div class="stat-icon dib"><i class="ti-money text-success border-success"></i></div>
                                <div class="stat-content dib">
                                    <div class="stat-text">Aprobadas</div>
                                    <div class="stat-digit">7</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-one">
                                <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                                <div class="stat-content dib">
                                    <div class="stat-text">Cursando</div>
                                    <div class="stat-digit">4</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-one">
                                <div class="stat-icon dib"><i class="ti-layout-grid2 text-warning border-warning"></i>
                                </div>
                                <div class="stat-content dib">
                                    <div class="stat-text">Restantes</div>
                                    <div class="stat-digit">17</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-one">
                                <div class="stat-icon dib"><i class="ti-link text-danger border-danger"></i></div>
                                <div class="stat-content dib">
                                    <div class="stat-text">total</div>
                                    <div class="stat-digit">28</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include('../fijos/footer.php');?>